CC:=clang
CFLAGS:=-Wall -Wextra -Wpedantic -std=c99 -O3

all: bin/life bin/colorize

bin/life: obj/life.o obj/grid.o obj/pattern.o obj/getvars.o
	$(CC) $(CFLAGS) $^ -o bin/life
bin/colorize: obj/colorize.o obj/grid.o obj/getvars.o
	$(CC) $(CFLAGS) $^ -o bin/colorize

obj/colorize.o: src/colorize.c
	$(CC) $(CFLAGS) -c $< -o $@
obj/life.o: src/life.c
	$(CC) $(CFLAGS) -c $< -o $@
obj/%.o: src/%.c src/%.h
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f obj/* bin/*
