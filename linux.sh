#!/bin/sh
sed -i 's/EFTYPE/EINVAL/g' src/grid.c &
# C99 standard seems to make gcc freak out, even though I only ever learnt C99
sed -i -e 's/clang/gcc/' -e 's/-std=c99//' Makefile
