/* Pattern picker and patterns to pick from. */
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#include "pattern.h"
#include "getvars.h"
#include "grid.h"

#define NUM_PATTERNS 8
#define LEN_STEP 4

#define CANT_FIT(){\
  printf("can't fit on grid - press enter to continue"); fflush(stdout);\
  getchar();\
  return true;\
}

typedef enum {NORTH, EAST, SOUTH, WEST} compass;

int get_direction(compass *direction);
int glider(int row, int column, int **gen, compass direction,
    int nrows, int ncols);
int lwss(int row, int column, int **gen, compass direction,
    int nrows, int ncols);
int diehard(int row, int column, int **gen, compass direction,
    int nrows, int ncols);
int century(int row, int column, int **gen, compass direction,
    int nrows, int ncols);

/*
 * patter_picker: prompts the user for where to put their selected pattern on
 * the grid.
 */
bool
pattern_picker(int *(**gen), int *nrows, int *ncols){
  int i, selection, row, column;
  compass direction;
  const char *patterns[NUM_PATTERNS] = {"glider", "lightweight spaceship",
    "die hard", "century", "pick individual cell", "kill cell",
    "import from file", "export to file"};
  char *p;

  for(i = 0; i < NUM_PATTERNS; i++)
    printf("%d. %s\n", i+1, patterns[i]);
  printf("Pattern to place on grid (0 for none)? ");
  while(true){
    if(int_from_file(&selection, stdin) < 0)
      if(errno == ENOMEM)
        exit(EX_OSERR);
    if(selection == 0)
      return false;
    if(selection > 0 && !(selection > NUM_PATTERNS))
      break;
    printf("invalid selection, try again: ");
  }

  if(selection == NUM_PATTERNS){
    FILE *file;
    printf("file name? ");
    if((p = getstr(LEN_STEP, stdin)) == NULL)
      exit(EX_OSERR);
    if((file = fopen(p, "w")) == NULL){
      fprintf(stderr, "*ERROR* failed to open file %s for writing\n", p);
      exit(EX_IOERR);
    }

    fprintf(file, "%dx%d\n", *nrows, *ncols);
    {
      int j;
      for(i = 0; i < *nrows; i++)
        for(j = 0; j < *ncols; j++)
          if((*gen)[i][j] == 1)
            fprintf(file, "%d,%d\n", j, i);
    }
    fclose(file);
    return true;
  }
  else if(selection == NUM_PATTERNS-1){
    FILE *file;
    printf("file name? ");
    if((p = getstr(LEN_STEP, stdin)) == NULL)
      exit(EX_OSERR);
    if((file = fopen(p, "r")) == NULL){
      fprintf(stderr, "*ERROR* failed to open file %s for reading\n", p);
      exit(EX_IOERR);
    }

    for(i = 0; i < *nrows; i++)
      free((*gen)[i]);
    free(*gen);
    if(grid_from_file(gen, file, nrows, ncols) < 0)
      exit(errno == ENOMEM ? EX_OSERR : EX_DATAERR);
    return true;
  }

  printf("Coordinates for selection? ");
  if((p = getstr(LEN_STEP, stdin)) == NULL)
    exit(EX_OSERR);
  while(get_sep_nums(p, &column, &row, ',') < 0 || (column < 0 || row < 0) ||
      (column >= *ncols || row >= *nrows)){
    if(errno == ENOMEM)
      exit(EX_OSERR);
    free(p);
    printf("invalid coordinate, try again: ");
    if(!(p = getstr(LEN_STEP, stdin)))
      exit(EX_OSERR);
  }

  switch(selection){
    case 1:
      printf("Direction (north-east is 0, incrementing and moving clockwise)? ");
      if(get_direction(&direction) < 0)
        exit(EX_OSERR);
      if(glider(row, column, *gen, direction, *nrows, *ncols) == 1)
        CANT_FIT();
      break;
    case 2:
      printf("Direction (north is 0, incrementing and moving clockwise)? ");
      if(get_direction(&direction) < 0)
        exit(EX_OSERR);
      if(lwss(row, column, *gen, direction, *nrows, *ncols) == 1)
        CANT_FIT();
      break;
    case 3:
      printf("Direction (north is 0, incrementing and moving clockwise)? ");
      if(get_direction(&direction) < 0)
        exit(EX_OSERR);
      if(diehard(row, column, *gen, direction, *nrows, *ncols) == 1)
        CANT_FIT();
      break;
    case 4:
      printf("Direction (north is 0, incrementing and moving clockwise)? ");
      if(get_direction(&direction) < 0)
        exit(EX_OSERR);
      if(century(row, column, *gen, direction, *nrows, *ncols) == 1)
        CANT_FIT();
      break;
    case 5:
      (*gen)[row][column] = 1; break;
    case 6:
      (*gen)[row][column] = 0; break;
  }
  return true;
}

int
get_direction(compass *direction){
  while(true){
    if(int_from_file((int*) direction, stdin) < 0)
      if(errno == ENOMEM)
        return -1;
    if(*direction >= 0 && *direction <= 3)
      break;
    printf("invalid direction, try again: ");
  }
  return 0;
}

int
glider(int row, int column, int **gen, compass direction, int nrows, int ncols){
  if(row+1 >= nrows || row-1 < 0 || column+1 >= ncols || column-1 < 0)
    return 1;
  // NORTH becomes north-east and move clockwise
  switch(direction){
    case NORTH:
      gen[row+1][column] = 1;
      gen[row][column+1] = 1;
      gen[row-1][column+1] = 1;
      gen[row-1][column] = 1;
      gen[row-1][column-1] = 1;
      break;
    case EAST:
      gen[row-1][column+1] = 1;
      gen[row][column+1] = 1;
      gen[row+1][column+1] = 1;
      gen[row+1][column] = 1;
    	gen[row][column-1] = 1;
    	break;
    case SOUTH:
  		gen[row-1][column] = 1;
    	gen[row][column-1] = 1;
    	gen[row+1][column-1] = 1;
    	gen[row+1][column] = 1;
    	gen[row+1][column+1] = 1;
    	break;
    case WEST:
      gen[row][column+1] = 1;
      gen[row-1][column] = 1;
      gen[row-1][column-1] = 1;
      gen[row][column-1] = 1;
      gen[row+1][column-1] = 1;
      break;
  }

  return 0;
}

int
lwss(int row, int column, int **gen, compass direction, int nrows, int ncols){
  switch(direction){
    case NORTH:
      if(row+2 >= nrows || row-2 < 0 || column+1 >= ncols || column-2 < 0)
        return 1;
      gen[row-1][column+1] = 1;
      gen[row+2][column+1] = 1;
      gen[row+2][column-1] = 1;
      gen[row+1][column-2] = 1;
      gen[row][column-2] = 1;
      gen[row-1][column-2] = 1;
      gen[row-2][column-2] = 1;
      gen[row-2][column-1] = 1;
      gen[row-2][column] = 1;
      break;
    case EAST:
      if(row+1 >= nrows || row-2 < 0 || column+2 >= ncols || column-2 < 0)
        return 1;
      gen[row-1][column-2] = 1;
      gen[row-2][column-1] = 1;
      gen[row-2][column] = 1;
      gen[row-2][column+1] = 1;
      gen[row-2][column+2] = 1;
      gen[row-1][column+2] = 1;
      gen[row][column+2] = 1;
      gen[row+1][column+1] = 1;
      gen[row+1][column-2] = 1;
      break;
    case SOUTH:
      if(row+2 >= nrows || row-2 < 0 || column+2 >= ncols || column-1 < 0)
        return 1;
      gen[row-2][column-1] = 1;
      gen[row-2][column+1] = 1;
      gen[row-1][column+2] = 1;
      gen[row][column+2] = 1;
      gen[row+1][column+2] = 1;
      gen[row+2][column+2] = 1;
      gen[row+2][column+1] = 1;
      gen[row+2][column] = 1;
      gen[row+1][column-1] = 1;
      break;
    case WEST:
      if(row+2 >= nrows || row-1 < 0 || column+2 >= ncols || column-2 < 0)
        return 1;
      gen[row-1][column+2] = 1;
      gen[row+1][column+2] = 1;
      gen[row+2][column+1] = 1;
      gen[row+2][column] = 1;
      gen[row+2][column-1] = 1;
      gen[row+2][column-2] = 1;
      gen[row+1][column-2] = 1;
      gen[row][column-2] = 1;
      gen[row-1][column-1] = 1;
      break;
  }

  return 0;
}

int
diehard(int row, int column, int **gen, compass direction, int nrows, int ncols){
  switch(direction){
    case NORTH:
      if(row+1 >= nrows || row-1 < 0 || column+4 >= ncols || column-3 < 0)
        return 1;
      gen[row][column-2] = 1;
      gen[row+1][column-2] = 1;
      gen[row][column-3] = 1;
      gen[row+1][column+2] = 1;
      gen[row+1][column+3] = 1;
      gen[row-1][column+3] = 1;
      gen[row+1][column+4] = 1;
      break;
    case EAST:
      if(row+4 >= nrows || row-3 < 0 || column+1 >= ncols || column-1 < 0)
        return 1;
      gen[row-2][column] = 1;
      gen[row-2][column-1] = 1;
      gen[row-3][column] = 1;
      gen[row+2][column-1] = 1;
      gen[row+3][column-1] = 1;
      gen[row+3][column+1] = 1;
      gen[row+4][column-1] = 1;
      break;
    case SOUTH:
      if(row+1 >= nrows || row-1 < 0 || column+3 >= ncols || column-4 < 0)
        return 1;
      gen[row][column+2] = 1;
      gen[row-1][column+2] = 1;
      gen[row][column+3] = 1;
      gen[row-1][column-2] = 1;
      gen[row-1][column-3] = 1;
      gen[row+1][column-3] = 1;
      gen[row-1][column-4] = 1;
      break;
    case WEST:
      if(row+3 >= nrows || row-4 < 0 || column+1 >= ncols || column-1 < 0)
        return 1;
      gen[row+2][column] = 1;
      gen[row+2][column+1] = 1;
      gen[row+3][column] = 1;
      gen[row-2][column+1] = 1;
      gen[row-3][column+1] = 1;
      gen[row-3][column-1] = 1;
      gen[row-4][column+1] = 1;
      break;
  }

  return 0;
}

int
century(int row, int column, int **gen, compass direction, int nrows, int ncols){
  switch(direction){
    case NORTH:
      if(row+1 >= nrows || row-1 < 0 || column+1 >= ncols || column-2 < 0)
        return 1;
      gen[row][column] = 1;
      gen[row-1][column] = 1;
      gen[row-1][column+1] = 1;
      gen[row][column-1] = 1;
      gen[row+1][column-1] = 1;
      gen[row][column-2] = 1;
      break;
    case EAST:
      if(row+1 >= nrows || row-2 < 0 || column+1 >= ncols || column-1 < 0)
        return 1;
      gen[row][column] = 1;
      gen[row-1][column] = 1;
      gen[row-1][column-1] = 1;
      gen[row-2][column] = 1;
      gen[row][column+1] = 1;
      gen[row+1][column+1] = 1;
      break;
    case SOUTH:
      if(row+1 >= nrows || row-1 < 0 || column+2 >= ncols || column-1 < 0)
        return 1;
      gen[row][column] = 1;
      gen[row+1][column] = 1;
      gen[row+1][column-1] = 1;
      gen[row][column+1] = 1;
      gen[row-1][column+1] = 1;
      gen[row][column+2] = 1;
      break;
    case WEST:
      if(row+2 >= nrows || row-1 < 0 || column+1 >= ncols || column-1 < 0)
        return 1;
      gen[row][column] = 1;
      gen[row+1][column] = 1;
      gen[row+1][column+1] = 1;
      gen[row+2][column] = 1;
      gen[row][column-1] = 1;
      gen[row-1][column-1] = 1;
      break;
  }

  return 0;
}
