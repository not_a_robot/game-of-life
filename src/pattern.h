#ifndef PATTERN_H
#define PATTERN_H

#include <stdbool.h>

bool pattern_picker(int *(**gen), int *nrows, int *ncols);

#endif
