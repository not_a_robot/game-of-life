#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <unistd.h>
#include <sysexits.h>
#include <string.h>
#include <errno.h>

#include "getvars.h"
#include "grid.h"
#include "pattern.h"

#define GRID_ROWS 16
#define GRID_COLS 16
#define INC 1 // how many rows/columns to add when reaching edges
#define LEN_STEP 4
// clears screen using ANSI escape codes
#define CLEAR() printf("\033[2J\033[1;1H")

void usage();

/*
 * main: prompts for various variables, prompts for initial pattern on grid and
 * goes through generations until it reaches the number of gen_expire of
 * generations
 */
int
main(int argc, char *argv[]){
  int **gen_curr, **gen_prev,
      gen_expire, row, column, i, opt, nrows = GRID_ROWS, ncols = GRID_COLS;
  float sleep_interval;
  bool forever = false, sleep_set = false, gen_set = false, print = true,
       dynamic = false, output = false, input = false, edit = false,
       config = false, fg_set = false, lfg_set = false, bg_set = false,
       lbg_set = false;
  struct timespec wait_time;
  FILE *out_file, *in_file, *conf_file;
  // default colours: black fg for dead, white for live, default bg for both
  Colours print_colours = {.fg = "30", .bg = "49", .lfg = "37", .lbg = "49"};

  while((opt = getopt(argc, argv, "s:g:o:i:F:f:B:b:c:Gndeh")) != -1){
    switch(opt){
      case 's':
        if(sleep_set)
          fprintf(stderr, "sleep interval set twice: set to %g\n",
              sleep_interval);
        else{
          if(getfloat(optarg, &sleep_interval) < 0)
            exit(errno == EINVAL ? EX_USAGE : EX_OSERR);
          if(sleep_interval < 0){
            fprintf(stderr, "sleep interval can't be negative\n");
            exit(EX_USAGE);
          }
          wait_time.tv_sec = (int) sleep_interval;
          wait_time.tv_nsec = (sleep_interval - (int) sleep_interval)
            * 1000000000;
          sleep_set = true;
        }
        break;

      case 'n':
        print = false;
        break;

      case 'g':
        if(gen_set)
          fprintf(stderr, "number of generations set twice: set to %d\n",
              gen_expire);
        else{
          if(getint(optarg, &gen_expire) < 0)
            exit(errno == EINVAL ? EX_USAGE : EX_OSERR);
          if(gen_expire < 0){
            fprintf(stderr, "expiry generation can't be negative\n");
            exit(EX_USAGE);
          }
          gen_set = true;
        }
        break;

      case 'G': // infinite generations?
        gen_expire = 0;
        gen_set = true;
        break;

      case 'd':
        dynamic = true;
        break;

      case 'o':
        if((out_file = fopen(optarg, "w")) == NULL){
          fprintf(stderr, "*ERROR* can't open file %s for writing\n", optarg);
          exit(EX_CANTCREAT);
        }
        output = true;
        break;

      case 'i':
        if((in_file = fopen(optarg, "r")) == NULL){
          fprintf(stderr, "*ERROR* can't open file %s for reading\n", optarg);
          exit(EX_NOINPUT);
        }
        input = true;
        break;

      case 'c':
        if((conf_file = fopen(optarg, "r")) == NULL){
          fprintf(stderr, "*ERROR* can't open file %s for reading\n", optarg);
          exit(EX_NOINPUT);
        }
        config = true;
        break;

      case 'e':
        edit = true;
        break;

      case 'b':
        GETCOLOUR(optarg, print_colours.bg, 'b');
        bg_set = true;
        break;

      case 'f':
        GETCOLOUR(optarg, print_colours.fg, 'f');
        fg_set = true;
        break;

      case 'B':
        GETCOLOUR(optarg, print_colours.lbg, 'b');
        lbg_set = true;
        break;

      case 'F':
        GETCOLOUR(optarg, print_colours.lfg, 'f');
        lfg_set = true;
        break;

      case 'h':
        usage();
        exit(EX_OK);
        break;

      default:
        usage();
        exit(EX_USAGE);
    }
  }

  if(config){
    int _gen_expire;
    char *in_string, *opt, *val;
    Colours temp_colours;
    bool _gen_set = false, _sleep_set = false, _fg_set = false, _bg_set = false,
         _lfg_set = false, _lbg_set = false;

    while((in_string = getstr(LEN_STEP, conf_file)) && *in_string != EOF){
      // ignore lines that only contain whitespace, or begin with '#'
      for(i = 0; isspace(in_string[i]); i++);
      if(in_string[i] == '#'){
        free(in_string); continue;
      }
      if(in_string[i] == '\0'){
        free(in_string); continue;
      }

      if(getseps(&in_string[i], &opt, &val, '=') < 0)
        exit(errno == EINVAL ? EX_DATAERR : EX_OSERR);

      if(strcmp(opt, "forever") == 0){
        if(strcmp(val, "true") == 0){
          _gen_expire = 0;
          _gen_set = true;
        }
        else if(strcmp(val, "false") == 0)
          ;
        else{
          fprintf(stderr, "Syntax error: invalid option value\n");
          exit(EX_DATAERR);
        }
      }
      else if(strcmp(opt, "print") == 0){
        if(strcmp(val, "true") == 0)
          ;
        else if(strcmp(val, "false") == 0)
          print = false;
        else{
          fprintf(stderr, "Syntax error: invalid option value\n");
          exit(EX_DATAERR);
        }
      }
      else if(strcmp(opt, "dynamic") == 0){
        if(strcmp(val, "true") == 0)
          dynamic = true;
        else if(strcmp(val, "false") == 0)
          ;
        else{
          fprintf(stderr, "Syntax error: invalid option value\n");
          exit(EX_DATAERR);
        }
      }

      else if(strcmp(opt, "gen_expire") == 0){
        if(getint(val, &_gen_expire) < 0)
          exit(errno == EINVAL ? EX_USAGE : EX_OSERR);
        _gen_set = true;
      }
      else if(strcmp(opt, "rows") == 0){
        if(getint(val, &nrows) < 0)
          exit(errno == EINVAL ? EX_USAGE : EX_OSERR);
      }
      else if(strcmp(opt, "columns") == 0){
        if(getint(val, &ncols) < 0)
          exit(errno = EINVAL ? EX_USAGE : EX_OSERR);
      }
      else if(strcmp(opt, "sleep_interval") == 0){
        if(getfloat(val, &sleep_interval) < 0)
          exit(errno = EINVAL ? EX_USAGE : EX_OSERR);
        if(sleep_interval < 0){
          fprintf(stderr, "sleep interval can't be negative\n");
          exit(EX_USAGE);
        }
        _sleep_set = true;
      }

      else if(strcmp(opt, "fg_colour") == 0 || strcmp(opt, "fg_color") == 0){
        GETCOLOUR(val, temp_colours.fg, 'f');
        _fg_set = true;
      }
      else if(strcmp(opt, "bg_colour") == 0 || strcmp(opt, "bg_color") == 0){
        GETCOLOUR(val, temp_colours.bg, 'b');
        _bg_set = true;
      }
      else if(strcmp(opt, "live_fg_color") == 0 || strcmp(opt, "lfg_color") == 0 ||
          strcmp(opt, "live_fg_colour") == 0 || strcmp(opt, "lfg_colour") == 0){
        GETCOLOUR(val, temp_colours.lfg, 'f');
        _lfg_set = true;
      }
      else if(strcmp(opt, "live_bg_color") == 0 || strcmp(opt, "lbg_color") == 0 ||
          strcmp(opt, "live_bg_colour") == 0 || strcmp(opt, "lbg_colour") == 0){
        GETCOLOUR(val, temp_colours.lbg, 'b');
        _lbg_set = true;
      }

      else{
        fprintf(stderr, "Syntax error: invalid config option\n");
        exit(EX_DATAERR);
      }

      free(in_string);
      free(opt);
      free(val);
    }
    if(!in_string)
      exit(EX_OSERR);

    // command-line options override config file
    if(!sleep_set && _sleep_set){
      wait_time.tv_sec = (int) sleep_interval;
      wait_time.tv_nsec = (sleep_interval - (int) sleep_interval) * 1000000000;
      sleep_set = true;
    }
    if(!gen_set && _gen_set){
      gen_expire = _gen_expire;
      gen_set = true;
    }
    // no need to set *g_set as true since they aren't used again
    if(!fg_set && _fg_set)
      strcpy(print_colours.fg, temp_colours.fg);
    if(!bg_set && _bg_set)
      strcpy(print_colours.bg, temp_colours.bg);
    if(!lfg_set && _lfg_set)
      strcpy(print_colours.lfg, temp_colours.lfg);
    if(!lbg_set && _lbg_set)
      strcpy(print_colours.lbg, temp_colours.lbg);
  }

  if(edit && !input){
    fprintf(stderr, "no input grid file to edit\n");
    usage();
    exit(EX_USAGE);
  }
  if(gen_expire == 0 && output){
    fprintf(stderr, "can't output to file when looping forever\n");
    usage();
    exit(EX_USAGE);
  }

  if(!sleep_set && print){
    printf("Pause before incrementing generation in seconds? ");
    while(true){
      if(float_from_file(&sleep_interval, stdin) < 0)
        if(errno == ENOMEM)
          exit(EX_OSERR);
      if(sleep_interval > 0)
        break;
      printf("invalid sleep interval, try again: ");
    }
    wait_time.tv_sec = (int) sleep_interval;
    wait_time.tv_nsec = (sleep_interval - (int) sleep_interval) * 1000000000;
  }
  if(!gen_set){
    printf("Generation number to expire on (0 to never expire)? ");
    while(true){
      if(int_from_file(&gen_expire, stdin) < 0)
        if(errno == ENOMEM)
          exit(EX_OSERR);
      if(gen_expire >= 0)
        break;
      printf("invalid expiry generation, try again: ");
    }
  }
  forever = gen_expire == 0;

    if(input){
      if(grid_from_file(&gen_curr, in_file, &nrows, &ncols) < 0)
        exit(errno == ENOMEM ? EX_OSERR : EX_DATAERR);
    }
    else if(!(gen_curr = make_grid(nrows, ncols)))
      exit(EX_OSERR);
    if(!(gen_prev = make_grid(nrows, ncols)))
      exit(EX_OSERR);

  if(!input || edit){
    do{
      CLEAR();
      printf("Grid now:\n");
      print_grid(stdout, gen_curr, true, &print_colours, nrows, ncols);
    }while(pattern_picker(&gen_curr, &nrows, &ncols));
  }

  // apply rules to change the grid, and print each generation individually
  // after the set sleep interval, unless instructed to only print the final
  // generation.
  for(i = 1; forever || i <= gen_expire; i++){
    copy_grid(gen_prev, gen_curr, nrows, ncols);

    for(row = 0; row < nrows; row++){
      for(column = 0; column < ncols; column++){
        if(dynamic){
          // add rows down when reaching the bottom edge
          if(gen_prev[row][column] && row+2 > nrows){
            resize_rows(&gen_prev, BEGINNING, INC, nrows, ncols);
            resize_rows(&gen_curr, BEGINNING, INC, nrows, ncols);
            nrows += INC;
          }
          // add rows up when reaching the upper edge
          if(gen_prev[row][column] && row-2 < 0){
            resize_rows(&gen_prev, END, INC, nrows, ncols);
            resize_rows(&gen_curr, END, INC, nrows, ncols);
            nrows += INC;
          }
          // add rows to the right when reaching the right edge
          if(gen_prev[row][column] && column+2 > ncols){
            resize_cols(gen_prev, BEGINNING, INC, nrows, ncols);
            resize_cols(gen_curr, BEGINNING, INC, nrows, ncols);
            ncols += INC;
          }
          // add rows to the left when reaching the left edge
          if(gen_prev[row][column] && column-2 < 0){
            resize_cols(gen_prev, END, INC, nrows, ncols);
            resize_cols(gen_curr, END, INC, nrows, ncols);
            ncols += INC;
          }
        }
        gen_curr[row][column] =
          live(gen_prev, row, column, nrows, ncols) ? 1 : 0;
      }
    }

    if(print){
      CLEAR();
      printf("Generation number: %d\n", i);
      print_grid(stdout, gen_curr, false, &print_colours, nrows, ncols);
      nanosleep(&wait_time, NULL);
    }
  }

  // print final generation when not printing every generation
  if(!print){
    CLEAR();
    printf("Generation number: %d\n", --i);
    print_grid(stdout, gen_curr, false, &print_colours, nrows, ncols);
  }

  if(output){
    int j;
    fprintf(out_file, "%dx%d\n", ncols, nrows);
    for(i = 0; i < nrows; i++)
      for(j = 0; j < ncols; j++)
        if(gen_curr[i][j] == 1)
          fprintf(out_file, "%d,%d\n", j, i);
    fclose(out_file);
  }

  for(i = 0; i < nrows; i++){
    free(gen_curr[i]);
    free(gen_prev[i]);
  }
  free(gen_curr);
  free(gen_prev);

  return 0;
}

void
usage(){
  fprintf(stderr,
  "usage: life [options]\n\
  -B [colour]\t\tspecify background colour of live cells\n\
  -F [colour]\t\tspecify foreground colour of live cells\n\
  -G\t\t\tcalculate generations indefinitely\n\
  -b [colour]\t\tspecify background colour of dead cells\n\
  -f [colour]\t\tspecify foreground colour of dead cells\n\
  -d\t\t\tdynamically resize the grid when live cells reach the edges (universe wraps around by default)\n\
  -e\t\t\topen the grid editor even when reading from a file\n\
  -g [generations]\tnumber of generatons to calculate\n\
  -h\t\t\tdisplays a summary of options accepted by life\n\
  -i [file]\t\tread grid from a file\n\
  -n\t\t\tonly print final generation\n\
  -o [file]\t\toutput the final generation to a file\n\
  -s [sleep interval]\tseconds to sleep between each print of a generation\n");
}
