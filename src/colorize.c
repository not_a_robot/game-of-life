/* colorizes the output file made by life */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <sysexits.h>
#include <string.h>

#include "grid.h"
#include "getvars.h"

#define LEN_STEP 4
#define RESET() printf("\033[0m")

void usage();

int
main(int argc, char *argv[]){
  int opt, i, nrows, ncols, **grid, err;
  // default colours: black fg for dead, white for live, default bg for both
  Colours colours = {.fg = "30", .bg = "49", .lfg = "37", .lbg = "49"};
  FILE *input_file;

  while((opt = getopt(argc, argv, "b:f:B:F:h")) != -1){
    switch(opt){
      case 'b':
        GETCOLOUR(optarg, colours.bg, 'b');
        break;
      case 'f':
        GETCOLOUR(optarg, colours.fg, 'f');
        break;
      case 'B':
        GETCOLOUR(optarg, colours.lbg, 'b');
        break;
      case 'F':
        GETCOLOUR(optarg, colours.lfg, 'f');
        break;
      case 'h':
        usage();
        return 0;
      default:
        usage();
        return EX_USAGE;
    }
  }

  if((input_file = fopen(argv[argc-1], "r")) == NULL){
    fprintf(stderr, "can't open file %s\n", argv[1]);
    return EX_NOINPUT;
  }

  if((err = grid_from_file(&grid, input_file, &nrows, &ncols)) != EX_OK)
    return err;
  fclose(input_file);

  print_grid(stdout, grid, false, &colours, nrows, ncols);

  for(i = 0; i < nrows; i++)
    free(grid[i]);
  free(grid);

  return 0;
}

void
usage(){
  printf("usage: colorize-life [options] [file]\n\
  -b\tbackground color\n\
  -f\tforeground color\n\
  -B\tlive background color\n\
  -F\tlive foreground color\n");
}
