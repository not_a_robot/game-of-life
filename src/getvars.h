#ifndef GETVARS_H
#define GETVARS_H

#include <stdlib.h>
#include <sysexits.h>

// get colour and exit on error; most common use case
#define GETCOLOUR(string, clr_str, ground){\
if(getcolour(string, clr_str, ground) < 0)\
  exit(EX_USAGE);\
}

int getint(const char *string, int *num);
float getfloat(const char *string, float *num);
int get_sep_nums(const char *string, int *num1, int *num2, char sep);
char *getstr(int len_step, FILE *stream);
char *strnows(const char *string);
int getseps(const char *string, char *(*opt), char *(*val), char sep);
int getcolour(const char *string, char *clr_str, char ground);
int int_from_file(int *num, FILE *stream);
int float_from_file(float *num, FILE *stream);

#endif
