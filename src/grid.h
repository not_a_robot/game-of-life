#ifndef GRID_H
#define GRID_H

#include <stdbool.h>

// whether to add grid elements at the beginning or end of a row/column
typedef enum {BEGINNING, END} Expand;
typedef struct Colours{
  char fg[3], bg[3], lfg[3], lbg[3];
} Colours;

bool live(int **gen, int row, int column, int nrows, int ncols);
void print_grid(FILE *stream, int **grid, bool coordinates,
    const Colours *colours, int nrows, int ncols);
void copy_grid(int **dst, int **src, int nrows, int ncols);
// pointer to a 2D array
void resize_rows(int *(**grid), Expand direction, int inc, int nrows, int ncols);
// don't need a pointer to the 2D array since we're addressing its elements
// (i.e. grid[i]) not changing the 2D array's address
void resize_cols(int **grid, Expand direction, int inc, int nrows, int ncols);
int grid_from_file(int *(**grid), FILE *file, int *nrows, int *ncols);
int **make_grid(int nrows, int ncols);

#endif
