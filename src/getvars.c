#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ctype.h>
#include <sysexits.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "getvars.h"

#define FG_PREFIX 3
#define BG_PREFIX 4

#define BLACK    0
#define RED      1
#define GREEN    2
#define YELLOW   3
#define BLUE     4
#define MAGENTA  5
#define CYAN     6
#define WHITE    7
#define DEFAULT  9

/*
 * getint: write the integer in the given string to num. Returns 0 on success,
 * -1 if else. Ignores whitespace using strnows, with an optional + or - sign
 *  placed before the number. errno set accordingly. 
 */
int
getint(const char *string, int *num){
  bool positive = true;
  char *no_ws;
  int tmp_num = 0;

  if(!(no_ws = strnows(string)))
    return -1;

  if(*no_ws == '+')
    no_ws++;
  else if(*no_ws == '-')
    no_ws++, positive = false;

  for(; *no_ws != '\0'; no_ws++){
    if(!isdigit(*no_ws))
      return fprintf(stderr, "getint: encountered non-digit\n"),
             errno = EINVAL, -1;
    if(positive)
      tmp_num = tmp_num * 10 + (*no_ws - '0');
    else
      tmp_num = tmp_num * 10 - (*no_ws - '0');
  }

  return *num = tmp_num, 0;
}

/*
 * getfloat: write the float value in the given string to num. Returns 0 on
 * success, -1 if else. Ignores whitespace using strnows, with an optional + or
 * - sign placed before the number. errno set accordingly.
 */
float
getfloat(const char *string, float *num){
  bool positive = true;
  char *no_ws;
  float tmp_num = 0;
  int divider = 1, period = 0;

  if(!(no_ws = strnows(string)))
    return -1;

  if(*no_ws == '+')
    no_ws++;
  else if(*no_ws == '-')
    no_ws++, positive = false;

  for(; *no_ws != '\0'; no_ws++){
    if(!isdigit(*no_ws) &&
        ((period += *no_ws == '.') >= 2 || period == 0)){
      return fprintf(stderr, "getfloat: non-digit unexpected\n"),
             errno = EINVAL, -1;
    }

    if(period == 1){ // skip over the current char (a full stop) only once
      period++;
      continue;
    }
    if(period == 2){
      divider *= 10;
      if(positive)
        tmp_num += (float) (*no_ws - '0') / divider;
      else
        tmp_num -= (float) (*no_ws - '0') / divider;
    }
    else{
      if(positive)
        tmp_num = tmp_num * 10 + (*no_ws - '0');
      else
        tmp_num = tmp_num * 10 - (*no_ws - '0');
    }
  }

  return *num = tmp_num, 0;
}

/*
 * get_sep_nums: gets two numbers from a string, separated by sep, and write
 * them to two int pointers. Returns 0 on success, -1 if else. No need to set
 * errno as other functions have already set it.
 */
int
get_sep_nums(const char *string, int *num1, int *num2, char sep){
  char *str1, *str2;

  if(getseps(string, &str1, &str2, sep) < 0)
    return -1;
  if(getint(str1, num1) < 0 || getint(str2, num2) < 0)
    return free(str1), free(str2), -1;

  return 0;
}

/*
 * getstr: get a line of an unkown size and return a pointer to it. Returns
 * NULL if memory allocation functions are to fail.
 */
char
*getstr(int len_step, FILE *stream){
  int i, len = len_step;
  char *p, ch;

  if(!(p = malloc(len * sizeof(char))))
    return fprintf(stderr, "getstr: malloc failed\n"), (NULL);

  for(i = 0; (ch = getc(stream)) != '\n'; i++){
    if(i >= len){
      len += len_step;
      if(!(p = realloc(p, len * sizeof(char))))
        return free(p), fprintf(stderr, "getstr: realloc failed\n"), (NULL);
    }
    p[i] = ch;
    if(ch == EOF)
      return p;
  }
  p[i] = '\0';

  return p;
}

/*
 * strnows: returns a pointer to a version of string minus whitespace before +
 * after word contained in string. Returns 0 on success, -1 if else. Sets errno
 * accordingly.
 */
char *
strnows(const char *string){
  char *newstr;
  int curr_pos, start_str;
  size_t len;

  for(curr_pos = 0; isspace(string[curr_pos]); curr_pos++);

  if(string[curr_pos] == '\0')
    return fprintf(stderr, "strnows: string only contains whitespace\n"),
           errno = EINVAL, newstr = NULL;
  start_str = curr_pos;

  for(; !isspace(string[curr_pos]) && string[curr_pos] != '\0'; curr_pos++);
  len = curr_pos - start_str;

  for(; string[curr_pos] != '\0'; curr_pos++)
    if(!isspace(string[curr_pos]))
      return fprintf(stderr, "strnows: string can only contain one word\n"),
             errno = EINVAL, newstr = NULL;

  newstr = strndup(string+start_str, len);

  if(!newstr) fprintf(stderr, "strnows: strndup failed\n"), errno = ENOMEM;
  return newstr;
}

/*
 * getseps: gets two strings separated by separator character. Ignores all
 * trailing whitespace, using strnows. Returns 0 on success, -1 if else. Sets
 * errno accordingly.
 */
int
getseps(const char *string, char *(*str1), char *(*str2), char sep){
  char *tmp_str;

  if(!(*str1 = strchr(string, sep)))
    return fprintf(stderr, "getseps: missing separator\n"), errno = EINVAL, -1;
  *str2 = *str1+1; // point right after occurence of sep

  if(!(tmp_str = strndup(string, *str1-string)))
    return fprintf(stderr, "getseps: strndup failed\n"), errno = ENOMEM, -1;

  if(!(*str1 = strnows(tmp_str)))
    return 0;
  free(tmp_str);

  if(!(*str2 = strnows(*str2)))
    return 0;

  return 0;
}

/* getcolour: get a human understandable (?) colour value from string, and
 * write its equivalent ANSI tty number for colour escape sequences to clr_str.
 * ground determines whether to write a foreground or background colour.
 * Returns 0 on success, -1 when an invalid colour is given, and -2 when an
 * invalid "ground" is given. errno set to EINVAL in either case.
 */
int
getcolour(const char *string, char *clr_str, char ground){
  int prefix;
  if(ground == 'b')
    prefix = BG_PREFIX;
  else if(ground == 'f')
    prefix = FG_PREFIX;
  else
    return fprintf(stderr, "getcolour: invalid ground given\n"),
           errno = EINVAL, -2;

  if(strcmp(string, "black") == 0)
    sprintf(clr_str, "%d%d", prefix, BLACK);
  else if(strcmp(string, "red") == 0)
    sprintf(clr_str, "%d%d", prefix, RED);
  else if(strcmp(string, "green") == 0)
    sprintf(clr_str, "%d%d", prefix, GREEN);
  else if(strcmp(string, "yellow") == 0)
    sprintf(clr_str, "%d%d", prefix, YELLOW);
  else if(strcmp(string, "blue") == 0)
    sprintf(clr_str, "%d%d", prefix, BLUE);
  else if(strcmp(string, "magenta") == 0)
    sprintf(clr_str, "%d%d", prefix, MAGENTA);
  else if(strcmp(string, "cyan") == 0)
    sprintf(clr_str, "%d%d", prefix, CYAN);
  else if(strcmp(string, "white") == 0)
    sprintf(clr_str, "%d%d", prefix, WHITE);
  else
    return fprintf(stderr, "getcolour: invalid colour given\n"),
           errno = EINVAL, -1;

  return 0;
}

/*
 * int_from_file/float_from_file: these small functions contain a way that
 * getstr and getint/getfloat are often used in conjunction, for convenience.
 */
int
int_from_file(int *num, FILE *stream){
  char *string;
  if(!(string = getstr(4, stream)))
    return -1;
  if(getint(string, num) < 0)
    return -1;
  return 0;
}

int
float_from_file(float *num, FILE *stream){
  char *string;
  if(!(string = getstr(4, stream)))
    return -1;
  if(getfloat(string, num) < 0)
    return -1;
  return 0;
}
