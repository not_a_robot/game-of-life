/* Functions related to manipulating 2D grids. */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>

#include "grid.h"
#include "getvars.h"

#define BLACK "\033[0;30m"
#define WHITE "\033[0;37m"

#define LEN_STEP 4

/* live: determines whether a cell at a given coordinates is to live or not */
bool
live(int **gen, int row, int column, int nrows, int ncols){
  // neighbours is -1 when alive because current cell counts as a "neighbour".
  int i, j, neighbours = gen[row][column] ? -1 : 0;

  for(i = -1; i <= 1; i++){
    int row_ = row; // temporary version to avoid permanent changes
    // wrap around...
    if(row_ + i < 0) row_ += nrows;
    else if(row_ + i >= nrows) row_ -= nrows;

    for(j = -1; j <= 1; j++){
      int column_ = column; // temporary version to avoid permanent changes
      // wrap around...
      if(column_ + j < 0) column_ += ncols;
      else if(column_ + j >= ncols) column_ -= ncols;

      if(gen[row_+i][column_+j])
        neighbours++;
    }
  }

  if(gen[row][column]){ // cell stays alive if it has 2 or 3 neighbours
    if(neighbours == 2 || neighbours == 3)
      return true;
  }
  else if(neighbours == 3) // cell comes to life if it has 3 neighbours
      return true;

  return false; // if neither criteria are met, the cell becomes/stays dead
}

/*
 * print_grid: print the selected grid, optionally printing coordinates around
 * the generation's grid.
 */
void
print_grid(FILE *stream, int **grid, bool coordinates, const Colours *colours,
    int nrows, int ncols){
  int i, j;

  if(coordinates){
    printf("   ");
    for(i = 0; i < ncols; i++)
      fprintf(stream, " %-2d", i);
  putchar('\n');
  }

  for(i = 0; i < nrows; i++){
    if(coordinates) fprintf(stream, "%-2d ", i);
    for(j = 0; j < ncols; j++){
      if(isatty(fileno(stream)))
        fprintf(stream, "\033[0;%s;%sm%2d ",
            (grid[i][j] ? colours->lbg : colours->bg),
            (grid[i][j] ? colours->lfg : colours->fg), grid[i][j]);
      else
        fprintf(stream, "%2d ", grid[i][j]);
    }
    if(isatty(fileno(stream)))
      fprintf(stream, "\033[0m\n"); // return to normal colour
    else
      putc('\n', stream);
  }
}

/* copy_grid: copy one grid to the other. */
void
copy_grid(int **dst, int **src, int nrows, int ncols){
  int *p1, *p2, i, j;
  for(i = 0, p1 = src[0], p2 = dst[0]; i < nrows;
      ++i, p1 = src[i], p2 = dst[i]){
    for(j = 0; j < ncols; j++)
      *p2++ = *p1++;
  }
}


void
resize_rows(int *(**grid), Expand direction, int inc, int nrows, int ncols){
  int i;

  switch(direction){
    case BEGINNING:
      *grid = realloc(*grid, sizeof(int *) * (nrows+inc));

      for(i = 0; i < inc; i++)
        (*grid)[nrows+i] = calloc(ncols, sizeof(int));
      break;

    case END:
      *grid = realloc(*grid, sizeof(int *) * (nrows+inc));

      for(i = nrows-1; i >= 0; i--) // move all rows forward by inc
        (*grid)[i+inc] = (*grid)[i];

      // allocate empty columns for first inc rows since they're unoccupied
      for(i = 0; i < inc; i++)
        for(int k = 0; k < ncols; k++)
          (*grid)[i] = calloc(ncols, sizeof(int));
      break;

    default:
      fprintf(stderr, "*ERROR* invalid direction given\n");
      exit(EXIT_FAILURE);
      break;
  }
}

void
resize_cols(int **grid, Expand direction, int inc, int nrows, int ncols){
  int i, j;

  switch(direction){
    // loop through each column in a row and add columns to the right
    case BEGINNING:
      for(i = 0; i < nrows; i++){
        grid[i] = realloc(grid[i], sizeof(int) * (ncols+inc));

        for(j = 0; j < inc; j++)
          grid[i][ncols+j] = 0;
      }
      break;

    // loop through each column in a row and add columns to the left
    case END:
      for(i = 0; i < nrows; i++){
        grid[i] = realloc(grid[i], sizeof(int) * (ncols+inc));

        // move all elements in a column forwards by inc
        for(j = ncols-1; j >= 0; j--)
          grid[i][j+inc] = grid[i][j];

        // zero out first inc elements of a column
        for(j = 0; j < inc; j++)
          grid[i][j] = 0;
      }
      break;

    default:
      fprintf(stderr, "*ERROR* invalid direction given\n");
      exit(EXIT_FAILURE);
      break;
  }
}

/*
 * grid_from_file: creates a 2d int grid using *alloc, and writes certain
 * coordinates as 1s based on the file given. The file format is described in
 * life(6). Returns 0 on success, -1 if else. errno is set accordingly.
 */
int
grid_from_file(int *(**grid), FILE *file, int *nrows, int *ncols){
  char *p;
  int row, col, i;

  while(true){
    if((p = getstr(LEN_STEP, file)) == NULL)
      return -1;
    // ignore lines that only contain whitespace, or begin with '#'
    for(i = 0; isspace(p[i]); i++);
    if(p[i] == '#'){
      free(p); continue;
    }
    if(p[i] == '\0'){
      free(p); continue;
    }
    if(get_sep_nums(p, ncols, nrows, 'x') < 0){
      if(errno == ENOMEM)
        return free(p), -1;
      fprintf(stderr, "Syntax error\n");
      return free(p), -1;
    }
    break;
  }
  free(p);

  *grid = make_grid(*nrows, *ncols);
  while(true){
    if((p = getstr(LEN_STEP, file)) == NULL)
      return -1;
    if(*p == EOF)
      break;

    for(i = 0; isspace(p[i]); i++);
    if(p[i] == '#'){
      free(p); continue;
    }
    if(p[i] == '\0'){
      free(p); continue;
    }

    if(get_sep_nums(&p[i], &col, &row, ',') < 0){
      if(errno == ENOMEM)
        return free(p), -1;
      fprintf(stderr, "Syntax error\n");
      return free(p), errno = EFTYPE, -1;
    }
    if(row >= *nrows || col >= *ncols){
      fprintf(stderr, "Syntax error: out of bounds\n");
      return errno = EFTYPE, -1;
    }
    
    free(p);
    (*grid)[row][col] = 1;
  }
  fclose(file);

  return 0;
}

/*
 * make_grid: return a pointer to a 2d grid of a specified size using *alloc.
 */
int **
make_grid(int nrows, int ncols){
  int **grid, i;

  if(!(grid = malloc(nrows * sizeof(int *))))
    return fprintf(stderr, "make_grid: malloc failed\n"), (NULL);
  for(i = 0; i < nrows; i++)
    if(!(grid[i] = calloc(ncols, sizeof(int))))
      return fprintf(stderr, "make_grid: calloc failed\n"), (NULL);

  return grid;
}
