# Conway's Game of Life
This is a text-based implementation of [Conway's Game of
Life](https://en.wikipedia.org/wiki/Conway's_Game_of_Life), using only the C
standard library.

## Rules
1. Any live cell with two or three live neighbours survives.
2. Any dead cell with three live neighbours becomes a live cell.
3. All other live cells die in the next generation. Similarly, all other dead
   cells stay dead.

## Features
- [X] A relatively basic grid editor for the initial pattern, including some
  pre-set patterns for ease of use.
  - [X] [Glider](https://conwaylife.com/wiki/Glider)
  - [X] [Lightweight
    Spaceship](https://conwaylife.com/wiki/Lightweight_spaceship)
  - [X] [Die hard](https://conwaylife.com/wiki/Die_hard)
  - [X] [Century](https://conwaylife.com/wiki/Century)
  - [X] Picking individual cells to give life to/kill.
- [X] Waits a user specified amount of seconds between each generation redraw,
  and stops at a user specified number of generations.
- [X] Choose between a finite universe that wraps around and a universe that
  expands indefinitely.
- [X] Command-line arguments to specify sleep interval and number of
  generations.
- [X] Command-line argument to output the final generation to a file.
- [X] Feed a pre-made grid file to the program to make it completely usable
  non-interactively.
- [X] A program that colorizes the output made by `life`, using custom 3-bit
  colours (using ANSI escape sequences).
- [X] Export current pattern in grid editor to a file.
- [X] 3-bit colours in `life`.
- [X] Config file for colours and other variables in the programs.
- [X] `man` pages for programs in this project.
- [X] Add ability to import from file in grid editor, rather than just command
  line option.
- [ ] A more effecient Game of Life algorithm.
  - Potential idea: keep track of live cells, and only check if they and the
    cells immediately around them only are to live or not.
- [ ] `ncurses` refactor; makes it much more interactive, rather than just
  inputting coordinates (also redrawing becomes more effecient).
- [ ] Add some sort of automated testing.
  - Might be more effort than it's worth since this is just a fun project, but
    manual testing is getting more and more annoying+error prone as the project
    gets bigger, and once I'm done with automated testing, I could focus more
    and getting stuff done... but then again this is literally just Yet Another
    Game Of Life Implementation.
- [ ] A file that defines custom structures to input in the grid editor.
  - i.e. the file would have a headings for each structure name, and
    coordinates relative to the selected cells are given under that definition.
- [ ] `man` pages for libraries in this project.

## Compilation Instructions

This assumes you have clang already installed (run `linux.sh` to run on linux
with gcc). Just run `make` and you should end up with two executables called
`life` and `colorize`, both in the `bin` directory.

## Known bugs

### None!
